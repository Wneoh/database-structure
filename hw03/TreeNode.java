/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw03;

/**
 * A class that define the what is in TreeNode and how it should be function.
 *
 * @author Wei Chin
 * @version 09/16/19
 */
public class TreeNode {

    TreeNode left, right;
    Word word;

    public TreeNode(Word word) {
        this.word = word;
    }

}
