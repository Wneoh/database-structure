/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw03;

/**
 * A class that named Word which store parsed String word and int count for
 * word.
 *
 * @author Wei Chin
 * @version 09/16/19
 */
public class Word {

    private String word;
    private int count;

    public Word(String word, int count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public int getCount() {
        return count;
    }

    public void inCount() {
        count++;
    }

}
