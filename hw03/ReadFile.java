/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * A ReadFile class that read the file, create MyRoot object and store the
 * parsed words in it accordingly.
 *
 * @author Wei Chin
 * @version 09/16/18
 */
public class ReadFile {

    MyRoot myRoot = new MyRoot(); // Create the MyRoot Object 
    Timer process = new Timer();
    Timer sort = new Timer();
    Timer total = new Timer();
   
    
    /**
     * A the parsed words into MyRoot, traverse through to add into Array, then
     * sort the Array and print the first 20 most count words.
     *
     * @param filename as the file that want to add the parsed word into MyRoot
     * @throws FileNotFoundException
     */
    public ReadFile(String filename) throws FileNotFoundException {
        process.start();
        File text = new File(filename);
        Scanner scar = new Scanner(text); // read the file
        while (scar.hasNext()) {
            String line = scar.nextLine();
            Word myWord = new Word(line,0);// create a new Word object
            if(myRoot.search(myWord)==null){ // check if the word has duplication
                myRoot.insert(myWord); // if not then insert each parsed word 
            }else{
                myRoot.search(myWord).word.inCount(); // esle get the location of the Treenode and increment the word.count
            }
        }
        myRoot.read(); // Traverse through the myRoot to add the word object into Array
        process.stop();
        sort.start();
        myRoot.selectionSort(); // Sort the array based on their count
        sort.stop();
        myRoot.display(); // Display the first 20 that has the most count words.
        System.out.println("Process time:"+process.toString());
        System.out.println("Sorting time:"+sort.toString());
        System.out.printf("Total time: %.3f",(sort.seconds()+process.seconds()));
        System.out.println("\n");
    }

}
