/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw03;

/**
 * A MyRoot class that use the word and TreeNode class to perform insert,
 * traverse, search and display.
 *
 * @author Wei Chin
 * @version 09/16/18
 */
public class MyRoot {

    private TreeNode n, p, q, root;
    private Word[] arrWords = new Word[15000];
    private int totalWords = 0;

    /**
     * Constructor that has the root set as null.
     */
    public MyRoot() {
        root = null;
    }

    /**
     * A method that insert parsed words into MyRoot.
     *
     * @param parsedWord as the word to insert into MyRoot
     */
    public void insert(Word parsedWord) {
        //Word word = new Word(parsedWord, 0); // create a new object word based on parsedWord and set count into 0
        n = new TreeNode(parsedWord); // new TreeNode that points to new word object
        n.left = null; // set new TreeNode left to null;
        n.right = null;// set new TreeNode right to null;
        p = root;
        while (p != null) {
            q = p; // have another pointer to follow p so that when p is null, q is the last TreeNode in MyRoot
            if (n.word.getWord().compareTo(p.word.getWord()) < 0) { // Compare the new TreeNode's word with the current TreeNode's word
                p = p.left;// if new TreeNode's word is smaller than the current TreeNode's word then currentNode will point to left 
            } else {
                p = p.right;// else point to right 
            }
        }
        if (root == null) { // if the root is null, then root will point to the new TreeNode 
            root = n;
        } else if (n.word.getWord().compareTo(q.word.getWord()) < 0) { // compare again to see if the new TreeNode's word is larger or smaller than the q TreeNode's word 
            q.left = n; // if smaller then new TreeNode will point to left
        } else {
            q.right = n; // if bigger then new TreeNode will point to right
        }
    }

    /**
     * A method that called Traverse method and add TreeNode's word into an
     * Array.
     */
    public void read() {
        p = root;
        traverse(p);
    }

    /**
     * A method that will print out 20 most counts word in the Array.
     */
    public void display() {
        for (int i = 0; i < 20; i++) {
            System.out.println("Word:'" + arrWords[i].getWord() + "'\tCounts: " + arrWords[i].getCount());
        }
    }

    /**
     * A method that sort the array based on the Word's count variable.
     */
    public void selectionSort() {
        for (int i = 0; i < totalWords - 1; i++) { // go through the list in the Array
            int max = i;// set the max to the first element
            for (int j = i + 1; j < totalWords; j++) { // have arrWords[j] to compare with arrWords[max]
                if (arrWords[max].getCount() < arrWords[j].getCount()) {// if the max's count is smaller than the arrWords[j]'s count 
                    max = j;// set the max to the arrWords[j]
                }
                Word temp = arrWords[max];// change the position of arrWords[j] and max 
                arrWords[max] = arrWords[i];
                arrWords[i] = temp;
            }
        }
    }

    /**
     * A method that will go through the MyRoot and add those TreeNode into
     * arrWords[].
     *
     * @param node
     */
    public void traverse(TreeNode node) {
        if (node != null) {
            traverse(node.left);
            arrWords[totalWords++] = node.word;
            traverse(node.right);
        }
    }

    /**
     * A method that accept Word object and compare it with the right location
     * of MyRoot for duplication.
     *
     * @param word as the object to see if there is any duplication
     * @return null if there is no duplication
     */
    public TreeNode search(Word word) {
        p = root;
        while (p != null) {
            if (p.word.getWord().equalsIgnoreCase(word.getWord())) {// if the current TreeNode's word's word is equal to para word's word 
                return p; // return the location of the TreeNode
            }
            if (p.word.getWord().compareTo(word.getWord()) < 0) {// if the current TreeNode's word's word is smaller to para word's word 
                p = p.right;// set the point to right to further investigate 
            } else {
                p = p.left;// set the point to left to further investigate 
            }
        }
        return null; // if there is no duplication return null 
    }
}
