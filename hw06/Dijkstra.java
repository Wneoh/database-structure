/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * A class that implement Dijkstra algorithm
 * @author weichin 
 * @version 11/19/18
 */
public class Dijkstra {

    PriorityQueue<Vertex> pq = new PriorityQueue<Vertex>();
    Vertex[] fArray;
    int source;
    int[][] route;
    int numOfVertex;

    public static void main(String[] args) throws FileNotFoundException {
        Dijkstra a = new Dijkstra(args[0]);

    }

    /**
     * A constructor that accept filename as parameter to interpret
     *
     * @param matrix as the file to read for the vertex and its cost
     * @throws FileNotFoundException
     */
    public Dijkstra(String matrix) throws FileNotFoundException {

        readMatrix(matrix);
        findTheShortestRoute();
    }

    /**
     * A method that will read matrix and store it into the respective variable
     *
     * @param matrix as the file name
     * @throws FileNotFoundException
     */
    public void readMatrix(String matrix) throws FileNotFoundException {
        int k;
        File inputfile = new File(matrix);
        Scanner sr = new Scanner(inputfile);
        numOfVertex = sr.nextInt();
        route = new int[numOfVertex][numOfVertex];
        fArray = new Vertex[numOfVertex];   // create an array that point to vertices that will be finalized in the end
        source = sr.nextInt();              // starting vertex
        k = 0;
        while (k < numOfVertex) {           // create vertices with MAX.Integer value except from starting source
            if (k == source) {
                fArray[k] = new Vertex(source, 0, true);
                pq.add(fArray[k]);
            } else {
                fArray[k] = new Vertex(k, Integer.MAX_VALUE, false);
                pq.add(fArray[k]);
            }
            k++;
        }
        sr.nextLine();
        while (sr.hasNextInt()) {           // store the matrix respectively to [][]route 
            for (int i = 0; i < route.length; i++) {
                for (int j = 0; j < route[i].length; j++) {
                    route[i][j] = sr.nextInt();
                }
            }
        }
    }

    /**
     * A method that implement Dijkstra algorithm
     */
    public void findTheShortestRoute() {
        int dx = 0;                     // starting vertex distance 
        int di = Integer.MAX_VALUE;;    // distance from x to i
        int wij = 0;                    // weight from i to j
        int dj = 0;                     // distance of j
        int i = 0;
        Vertex smallestDistance;        // smallest distance to check with 
        while (!pq.isEmpty()) {
            smallestDistance = pq.poll();   // get the minimum distance
            smallestDistance.setSettled();  // set it to be settled so that it won't check again
            i = smallestDistance.id;
            for (int j = 0; j < numOfVertex; j++) {     // go through the [][]route to figure the shortest path
                wij = route[smallestDistance.id][j];    // the weight of i to j that was read from the matrix file
                if (wij != 0 && !fArray[j].settled) {   // if the weight isn't 0 and the vertex in the fArray is not settled
                    di = fArray[i].dist;                // get the i distance from the source 
                    dj = fArray[j].dist;                // get the j distance from i 
                    if (dj > di + wij) {                // check if the distance of j is more than distance of i plus weight from i to j
                        fArray[j].prev = smallestDistance;  // if yes set the previous to the minimal distance that pulled from queue
                        dj = di + wij;                      // change the value to shorter distance 
                        pq.remove(fArray[j]);               // update the vertex's distance 
                        fArray[j].dist = dj;
                        pq.add(fArray[j]);                  
                    }
                }
            }
        }
        printDot();
    }

    /**
     * A method that create a dot output 
     */
    public void printDot() {
        System.out.println("graph solution {");
        System.out.println("\tnode [shape=\"circle\", fontsize=\"6\", width=\"0.6\", fixedsize=\"true\"];");
        for (int i = 0; i < numOfVertex; i++) {
            if (i == source) {
                System.out.println("\tR" + i + " [label=\"R" + i + "\\n" + fArray[i].dist + " ms\", shape=\"doublecircle\"];");
            } else {
                System.out.println("\tR" + i + " [label=\"R" + i + "\\n" + fArray[i].dist + " ms\"];");
            }
        }
        for (int k = 0; k < route.length; k++) {        // go through the route[][] to check connections
            for (int j = 0; j < route[k].length; j++) {
                if (route[k][j] != 0) {                 // if route[k][j] != 0, that means there is connection
                    if (j > k) {                        // this statement check cut the 2d into diagonally half which check each route[k][j] once
                        if ((fArray[k].prev == fArray[j]) || (fArray[j].prev == fArray[k])) {   // if there the prev of the vertex[k] is vertex[j] or the other way 
                            System.out.println("\tR" + k + "--R" + j + " [style=\"bold\"];");     // then they have a bold connection
                        } else {
                            System.out.println("\tR" + k + "--R" + j + " [style=\"dotted\"];");   // else they have a dotted connection 
                        }
                    }
                }
            }
        }
        System.out.println("}");
    }
}
