package hw05;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * A class that uses HashByWord and HashByPronun class to read and parse word
 * from selected files.
 *
 * @author weichin
 * @version 10/04/18
 *
 * <p>
 * In this class, word object will be created, read, parse dictionary.txt and
 * then insert into Hash table. Encode parsed words with Metaphone3,insert parsed
 * and encoded word into Word object. Insert the inserted Word object into
 * Hash table which created a Hash table of dictionary words. Then read, parsed
 * Melville.txt, pass the string as para to Hash table search method to find
 * suggestions.
 * </p>
 */
public class CheckSpell {

    private HashByWord dictWord = new HashByWord(); // Linedlist created
    private HashByPronun pronunWord = new HashByPronun();
    private Timer l = new Timer(); //Loading time
    private Timer p = new Timer(); //Parsing time
    private int numOfDictWord, wordNotFound, checked, correct = 0; // indicate how many words in Dictionary

    public void readDict(String filename) throws FileNotFoundException {
        l.start();
        File text = new File(filename);
        Scanner scar = new Scanner(text); // read the file
        while (scar.hasNext()) {
            String line = scar.nextLine();
            Word word = encode(line);
            dictWord.insert(word);
            pronunWord.insert(word);
            numOfDictWord++;
        }
        scar.close();			// close the file
        l.stop();
    }

    /**
     * A method that will accept a String and return the String with its pronunciation 
     * @param word as the word that use to find the pronunciation 
     * @return word object that contain the String word and String pronunciation 
     */
    public Word encode(String word) {
        Metaphone3 m3 = new Metaphone3(); // Metaphone3 object created
        m3.SetEncodeVowels(true);
        m3.SetWord(word);
        m3.Encode();				// encode the string line
        Word wordObject = new Word(word, m3.GetMetaph()); // create Word object, insert and encoded line into Word object
        return wordObject;
    }

    /**
     * Parse the given filename and search suggestions for the parsed word by
     * using Hash table search method.
     *
     * @param filename as the file to parse and search for suggestions
     * @throws FileNotFoundException to avoid any exception or error
     */
    public void checkFile(String filename) throws FileNotFoundException {
        p.start();
        File text = new File(filename);
        Scanner f = new Scanner(text);
        int i;
        String[] splitted; // the parsed word
        String str;// all the words from the file
        while (f.hasNext()) {
            str = f.nextLine();
            str = str.replaceAll("[^a-zA-Z]", " "); // get rid of non-alphanumeric
            splitted = str.split("\\s+");          // split based on whitespace
            for (i = 0; i < splitted.length; i++) {
                if ((splitted[i].length() > 2 && !splitted[i].matches(".*[A-Z].*"))) { // extracted lowercase words from melville.txt
                    if (dictWord.search(splitted[i])!= null) {// search for suggestions, if it's null, means the search word is in dictionary
                        checked++; // track num of checked words
                        correct++; // track the correct words
                    } else {
                        System.out.println("Suggestions for '" + splitted[i] + "': " + pronunWord.search(splitted[i]));
                        checked++; // add check too because I checked it and return suggestions
                    }
                }
            }
        }
        wordNotFound= checked - correct;// take the total checked minus the correct spelled word to get misspelled word
        f.close();
        p.stop();

        System.out.println("\nWords in Dictionary: " + numOfDictWord);
        System.out.println("Words Checked: " + checked);
        System.out.println("Words not found: " +wordNotFound);
        System.out.printf("Time Spent Loading: %.2f", l.seconds());
        System.out.printf("\nTime Spent Parsing: %.2f", p.seconds());
        System.out.printf("\nTotal Time Spent: %.2f", Float.parseFloat(l.toString()) + Float.parseFloat(p.toString()));
        System.out.println("\nNumbers of words in Hashtable (n): " + numOfDictWord);
        System.out.println("the size/capacity of the hash table (m): " + pronunWord.size());
        System.out.printf("the load factor: %.3f",((float)numOfDictWord/pronunWord.size()));
        System.out.println("\nthe length of the longest list: " + pronunWord.findTheLongestList());;
        System.out.println("the number of lists (not null): " + pronunWord.traverse());

    }
}
