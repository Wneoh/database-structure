/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw05;

/**
 * A subclass of WordHash that create a word Hash table and use as dictionary
 * operations.
 *
 * @author weichin
 * @version 10/04/18
 */
public class HashByWord extends WordHash {

    ListNode[] wordHash;
    int n=0;

    /**
     * A constructor that declare the size of wordHash table.
     */
    public HashByWord() {
        wordHash = new ListNode[20000];
    }

    /**
     * An override method that use word instead of pronunciation for the hash methods to get position in the wordHash table
     * @param word used as a word to get unique number position in the wordHash table
     * @return wordHash table position 
     */
    @Override
    public int hash(Word word) {
        return hashA(word.getWord()) % wordHash.length;
    }

    /**
     *  An override method that insert the word object into wordHash table
     * @param word as a word to insert into wordHash table
     */
    @Override
    public void insert(Word word) {
        ListNode p;
        int k;
        p = new ListNode(word);
        k = hash(word);
        p.next = wordHash[k];
        wordHash[k] = p;
        n++;
    }

    /**
     * An override method find if the searched word is an actual words compared to wordHash table element 
     * @param word as the word to check if it's an actual word 
     * @return null if it's not an actual words else return suggestions. 
     */
    @Override
    public String search(String word) {
        Word wordObject = encode(word);
        int k = hash(wordObject);
        ListNode p = wordHash[k];
        if (p == null) {
            return null;
        }
        while (p != null) {
            if (p.word.getWord().compareTo(word) == 0) {
                return p.word.getWord();
            }
            p=p.next;
        }
        return null;
    }
    /**
     * A methods that goes through each ListNode in Hash table to find the
     * longest List
     *
     * @return the longest Lists
     */
    public int findTheLongestList() {
        int max = 0;
        int temp = 1;
        for (int i = 0; i < wordHash.length; i++) {
            ListNode p = wordHash[i];
            temp = 0;
            while (p != null) {
                temp++;
                p = p.next;
            }
            if (temp > max) {
                max = temp;
            }

        }
        return max;
    }

    /**
     * A method that goes through the listNode in Hast table to find the number
     * of list that is not null
     *
     * @return the number of list in the Hash table that is not null
     */
    public int traverse() {
        int notnull = 0;
        for (int i = 0; i < wordHash.length; i++) {
            if (wordHash[i] != null) {
                notnull++;
            }
        }
        return notnull;
    }

    
    /**
     *  A method that returns the size of the hash table 
     * @return the size of the hash table
     */
    public int size(){
        return wordHash.length;
    }
}
