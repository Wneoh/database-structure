
package hw05;

/**
 * An abstract that holds the properties and methods of the hash functions for two subclasses.
 * 
 * @author weichin
 * @version 10/04/18
 */
public abstract class WordHash {
   
    public abstract int hash(Word word);
      public abstract void insert(Word word);
     public abstract String search (String word);
    
    /**
     * A built in hash function that return a unique number for the position in Hash table
     * @param word as the word to encode into a hash number 
     * @return a unique number for the position of the word in hash table
     */
    public int hashA(String word){
        return Math.abs(word.hashCode());
    }
    
    /**
     * A hash function that return a unique number for the position in Hash table
     * @param word as the word to encode into a hash number 
     * @return a unique number for the position of the word in hash table
     */
    public int hashB(String word){
        int i, sum ;
        sum =1;
        for (i =0;i<word.length();i++){
                sum*=(int) word.charAt(i);
        }
        return Math.abs(sum);
    }

    /**
     *A hash function that return a unique number for the position in Hash table
     * @param word as the word to encode into a hash number 
     * @return a unique number for the position of the word in hash table
     */
    public int hashC(String word){
        int hash = 19;
        for (int i = 0; i < word.length(); i++) {
        hash = hash*31 + (int) word.charAt(i);
        }
        return Math.abs(hash);

    }
    

    /**
     * A method to encode a string word into an object. 
     * @param word as the word to encode into a word object 
     * @return return the word object 
     */
    public Word encode(String word){
        Metaphone3 m3 = new Metaphone3();
        m3.SetEncodeVowels(true);
        m3.SetWord(word);
        m3.Encode();
        Word word1 = new Word(word,m3.GetMetaph());
        return word1;
    }
 
}
