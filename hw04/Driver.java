package hw05;

import java.io.FileNotFoundException;

/**
 * A class that contains main method to run the program.
 *
 * @author weichin
 * @version 10/04/18
 */
public class Driver {

    public static void main(String args[]) throws FileNotFoundException {
        CheckSpell a = new CheckSpell();
        a.readDict("C:\\Users\\weich\\Desktop\\DS\\src\\hw05\\dictionary.txt");
        a.checkFile("C:\\Users\\weich\\Desktop\\DS\\src\\hw05\\melville.txt");
    }
}
