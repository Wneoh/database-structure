package hw05;
/**
 * A class that named Word which store word and encoded word as an object.
 *
 * @author Weichin
 * @version 08/31/18
 *
 */
public class Word{
	private String word,pronunciation;

	/**
	 * Constructor that need two para to create this object
	 * @param word as the parsed word
	 * @param pronunciation as the encoded parsed word
	 */
	public Word(String word, String pronunciation){
		this.word=word;
		this.pronunciation = pronunciation;
	}

	/**
	 * Method to set word
	 *
	 * @param word as the parsed word
	 */
	public void setWord(String word) {
		this.word =word;
	}

	/**
	 * Method to set pronunciation
	 *
	 * @param pronunciation as the encoded word
	 */
	public void setPronunciation(String pronunciation) {
		this.pronunciation =pronunciation;
	}

	/**Method to get word
	 * @return word as the parsed word
	 */
	public String getWord() {
		return word;
	}

	/**
	 * Method to get pronunciation
	 *
	 * @return pronunciation as the encoded word
	 */
	public String getPronunciation() {
		return pronunciation;
	}
}
