/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw05;

/**
 *
 * @author weich
 */
public class WordsHash extends WordHash{
    MyLinkedList [] wordHash;
    
    public WordsHash(){
        wordHash = new MyLinkedList[20000];
    }
    
    @Override
    public int hash(String pronun) {
        int i, sum ;
        sum =1;
        for (i =0;i<pronun.length();i++){
                sum*=(int) pronun.charAt(i);
        }
        return Math.abs(sum)%wordHash.length;
    }
    
    public int hashA(String pronun){
        return Math.abs(pronun.hashCode())%wordHash.length;
    }
    
    public void insert(String word){
        wordHash[hash(word)].insert(word);
    } 
}


