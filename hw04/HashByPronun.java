/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw05;

/**
 * A subclass of WordHash that create a Pronunciation Hash table and use as
 * dictionary operations.
 *
 * @author weichin
 * @version 10/04/18
 */
public class HashByPronun extends WordHash {

    ListNode[] pronunHash;

    /**
     * A constructor that declare the size of pronunciationHash table.
     */
    public HashByPronun() {
        pronunHash = new ListNode[20000];
    }

    /**
     *
     * An override method that use pronunciation for the hash methods to get
     * position in the pronunciationHash table
     *
     * @param word used as a word to get unique number position in the
     * pronunciationHash table
     * @return pronunciationHash table position
     */
    @Override
    public int hash(Word word) {
        return hashA(word.getPronunciation()) % pronunHash.length;
    }

    /**
     * An override method that insert the word object into pronunciationHash
     * table
     *
     * @param word as a word to insert into pronunciationHash table
     */
    @Override
    public void insert(Word word) {
        ListNode p;
        int k;
        p = new ListNode(word);
        k = hash(word);
        p.next = pronunHash[k];
        pronunHash[k] = p;
    }

    /**
     *
     * An override method find if suggestions for words that could not find in
     * dictionary in pronunciationHash table
     *
     * @param word as the word to check
     * @return null if there is no suggestions else return suggestions.
     */
    @Override
    public String search(String word) {
        Word word1 = encode(word);
        int k = hash(word1);
        String suggestions = "";
        ListNode p = pronunHash[k];
        if (p == null) {
            return " ";
        }
        while (p != null) {
            if (word1.getPronunciation().compareTo(p.word.getPronunciation()) == 0) {
                suggestions = suggestions + " " + p.word.getWord();
            }
            p = p.next;
        }
        return suggestions;
    }

    /**
     * A methods that goes through each ListNode in Hash table to find the
     * longest List
     *
     * @return the longest Lists
     */
    public int findTheLongestList() {
        int max = 0;
        int temp = 1;
        for (int i = 0; i < pronunHash.length; i++) {
            ListNode p = pronunHash[i];
            temp = 0;
            while (p != null) {
                temp++;
                p = p.next;
            }
            if (temp > max) {
                max = temp;
            }

        }
        return max;
    }

    /**
     * A method that goes through the listNode in Hast table to find the number
     * of list that is not null
     *
     * @return the number of list in the Hash table that is not null
     */
    public int traverse() {
        int notnull = 0;
        for (int i = 0; i < pronunHash.length; i++) {
            if (pronunHash[i] != null) {
                notnull++;
            }
        }
        return notnull;
    }

    /**
     * A method that returns the size of the hash table
     *
     * @return the size of the hash table
     */
    public int size() {
        return pronunHash.length;
    }

}
