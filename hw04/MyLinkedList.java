package hw05;


/**
 * A MyLinkedList class that insert word into it,display ListNode in MyLinkedlist and search for suggestions.
 *
 * @author WeiChin
 * @version 08/31/18
 *
 */
public class MyLinkedList {
	private ListNode head;

	public MyLinkedList(){
		this.head =null;
	}

	/**
	 * A method that insert word into MyLinkedList from the front.
	 *
	 * @param word as the word object to insert into MyLinkedList
	 */
	public void insert(Word word) {
		ListNode temp = new ListNode(word); // create a new ListNode with para word in it.
		ListNode p = head;
		temp.next = p;// change the new ListNode next to p pointer
		head =temp;// change the head to new ListNode
	}

	/**
	 * A method that display all the ListNodes in MyLinkedList.
	 */
	public void display(){
		ListNode p = head;
		while(p!=null){
			System.out.println(p.word.toString());
			p =p.next;
		}
	}

	/**
	 * A method that search suggestions for @para word by encode it and compare with the encoded word in LinkedList.
	 * @param word as the word to search suggestions for
	 * @return
	 */
	public String search(String word) {
		String total =""; // add up all the suggestions and return it
		String empty ="";// return empty if there is not suggestions
		ListNode p = head;
		Metaphone3 m3 = new Metaphone3();
		m3.SetEncodeVowels(true);
		m3.SetWord(word);
		m3.Encode();
		boolean foundSug =false; // Keep track on if there is any suggestions for the para word
		while(p!=null){
			if(word.equals(p.word)) {
				return null;
			}
		p =p.next;
		}
		p =head;
		while(p!=null){
			if(m3.GetMetaph().equals(p.word)) { // compare to see if encoded para word and LinkedList encoded word are equal.
				total =total+""+p.word+" "; // if yes then print
				foundSug = true; // then pointer will indicate that it's found.
			}
		p =p.next;
		}
		if(foundSug==true) {// if there is suggestions for the search word then return the add up string 
			return total;
		}
		if(foundSug==false) { // if pointer is false, it means there is no suggestions for para word
			empty ="";
			return empty;
		}
		return null;
	}
	
}



 
