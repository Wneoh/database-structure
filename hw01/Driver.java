import java.io.FileNotFoundException;

/**
 * A class that contains main method to run the program.
 *
 * @author weichin
 * @version 08/31/18
 */
public class Driver {
	public static void main(String args[]) throws FileNotFoundException {
		CheckSpell a =new CheckSpell();
		a.readDict("C:\\Users\\weich\\Desktop\\Data Structure\\src\\database-structure\\data\\dictionary.txt");
		a.checkFile("C:\\Users\\weich\\Desktop\\Data Structure\\src\\database-structure\\data\\melville.txt");
	}
}
