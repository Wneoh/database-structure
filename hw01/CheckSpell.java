import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * A class that uses MylinkedList class to read and parse word from selected files.
 *
 * @author weichin
 * @version 08/31/18
 *
 * <p>
 * In this class, Mylinkedlist and word object will be created, then read and parse dictionary.txt.
 * Encode parsed words with Metaphone3,insert parsed and encoded word into Word object.
 * Insert the inserted Word object into Mylinkedlist which created a linked list of dictionary words.
 * Then read, parsed Melville.txt, pass the string as para to Linkedlist search method to find suggestions.
 * </p>
 */
public class CheckSpell {
	private MyLinkedList dict = new MyLinkedList(); // Linedlist created
	private Timer l = new Timer(); //Loading time
	private Timer p = new Timer(); //Parsing time
	private int dictWord,misspell,checked,correct =0; // indicate how many words in Dictionary
	private String preWord ="";
	
	
	public void readDict(String filename) throws FileNotFoundException {
		l.start();
		File text = new File(filename);
		Scanner scar = new Scanner(text); // read the file
		Metaphone3 m3 = new Metaphone3(); // Metaphone3 object created
		m3.SetEncodeVowels(true);
		while(scar.hasNext()) {
			String line = scar.nextLine();
			m3.SetWord(line);
			m3.Encode();				// encode the string line
			Word word = new Word(line,m3.GetMetaph()); // create Word object, insert and encoded line into Word object
			dict.insert(word);
			dictWord++;
		}
		scar.close();			// close the file
		l.stop();
	}

	/**
	 * Parse the given filename and search suggestions for the parsed word by using Linkedlist search method.
	 *
	 * @param filename as the file to parse and search for suggestions
	 * @throws FileNotFoundException to avoid any exception or error
	 */
	public void checkFile(String filename) throws FileNotFoundException {
		p.start();
		File text = new File(filename);
		Scanner f = new Scanner(text);
		int i;
		String [] splitted; // the parsed word
		String str;// all the words from the file
		while(f.hasNext()) {
			str= f.nextLine();
			str= str.replaceAll("[^a-zA-Z]", " "); // get rid of non-alphanumeric
			splitted = str.split("\\s+");          // split based on whitespace
			for (i=0; i<splitted.length; i++) {
				if ((splitted[i].length() > 2 && !splitted[i].matches(".*[A-Z].*"))) { // extracted lowercase words from melville.txt
					if(dict.search(splitted[i])==null) {// search for suggestions, if it's null, means the search word is in dictionary
						checked++; // track num of checked words
						correct++; // track the correct words
					}else{
						if(splitted[i].equals(preWord)) {// try to reduce duplication by comparing to the preWord. If there is then don't search it
							checked++;// add check too because I checked it with preWord
						}else{// but it will not detect duplication if there is one duplication before two searched words
							preWord = splitted[i];// initialize preWord as "" first then add value to it as parsing more word
							System.out.println("Suggestions for '"+splitted[i]+"': "+dict.search(splitted[i]));
							checked++; // add check too because I checked it and return suggestions
						}
					}
				}	
			}
		}
		misspell = checked -correct;// take the total checked minus the correct spelled word to get misspelled word
		f.close();
		p.stop();

		System.out.println("\nWords in Dictionary: "+dictWord);
		System.out.println("Words Checked: "+checked);
		System.out.println("Words not found: "+misspell);
		System.out.printf("Time Spent Loading: %.2f",l.seconds());
		System.out.printf("\nTime Spent Parsing: %.2f",p.seconds());
		System.out.printf("\nTotal Time Spent : %.2f",Float.parseFloat(l.toString())+Float.parseFloat(p.toString()));
		
	}
}
