/**
 *
 * A class that define what the ListNode has.
 *
 * @author weichin
 * @version 08/31/18
 */
public class ListNode {
	Word word;
	ListNode next;
	
	public ListNode(Word word){
		this.word =word;
	} // Constructor that require word object in ListNode.
}
