/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw05;

/**
 *
 * @author weichin
 * @version 10/22/18
 */
public class RBTree1 extends RBTree<BibleWord> {
    private BibleWord[] arrBibleWords = new BibleWord[15000];
    private int totalBibleWords = 0;

    /**
     * Constructor that has the root set as null.
     */
    public RBTree1() {
        super();
    }

    /**
     * A method that called Traverse method and add TreeNode's BibleWord into an
     * Array.
     */
    public void traverse() {
        traverse1(root);
    }

    /**
     * A method that will print out 20 most counts BibleWord in the Array.
     */
    public void displayTop20() {
        for (int i = 0; i < 20; i++) {
           System.out.println("BibleWord: "+arrBibleWords[i].toString());
        }
    }

    /**
     * A method that sort the array based on the BibleWord's count variable.
     */
    public void selectionSort() {
        for (int i = 0; i < totalBibleWords - 1; i++) { // go through the list in the Array
            int max = i;// set the max to the first element
            for (int j = i + 1; j < totalBibleWords; j++) { // have arrBibleWords[j] to compare with arrBibleWords[max]
                if (arrBibleWords[max].getCount()< arrBibleWords[j].getCount()) {// if the max's count is smaller than the arrBibleWords[j]'s count 
                    max = j;// set the max to the arrBibleWords[j]
                }
                BibleWord temp = arrBibleWords[max];// change the position of arrBibleWords[j] and max 
                arrBibleWords[max] = arrBibleWords[i];
                arrBibleWords[i] = temp;
            }
        }
    }

    /**
     * A method that will go through the MyRoot and add those TreeNode into
     * arrBibleWords[].
     *
     * @param node
     */
    private void traverse1(RBTreeNode node) {
        if (node == NIL) {
            return;
        }
            traverse1(node.left);
            arrBibleWords[totalBibleWords++] = (BibleWord) node.data;
            traverse1(node.right);
        }
   }

    /**
     * A method that accept BibleWord object and compare it with the right location
     * of MyRoot for duplication.
     *
     * @param BibleWord as the object to see if there is any duplication
     * @return null if there is no duplication
     */
    /*
    public TreeNode search(BibleWord BibleWord) {
        p = root;
        while (p != null) {
            if (p.BibleWord.getBibleWord().equalsIgnoreCase(BibleWord.getBibleWord())) {// if the current TreeNode's BibleWord's BibleWord is equal to para BibleWord's BibleWord 
                return p; // return the location of the TreeNode
            }
            if (p.BibleWord.getBibleWord().compareTo(BibleWord.getBibleWord()) < 0) {// if the current TreeNode's BibleWord's BibleWord is smaller to para BibleWord's BibleWord 
                p = p.right;// set the point to right to further investigate 
            } else {
                p = p.left;// set the point to left to further investigate 
            }
        }
        return null; // if there is no duplication return null 
    }
    */
    /*
    public RBTreeNode search(BibleWord BibleWord) {
        return searchR(BibleWord, root);
    }

    private RBTreeNode searchR(BibleWord BibleWord, RBTreeNode p) {
        if (p == null) {
            return null;
        }
        if (p.data.toString().equals(BibleWord.toString())) {// if the current TreeNode's BibleWord's BibleWord is equal to para BibleWord's BibleWord 
            return p; // return the location of the TreeNode
        }
        if (p.data.toString().compareTo(BibleWord.toString()) < 0) {// if the current TreeNode's BibleWord's BibleWord is smaller to para BibleWord's BibleWord 
            return searchR(BibleWord, p.right);
        } else {
            return searchR(BibleWord, p.left);
        }
    }
*/

