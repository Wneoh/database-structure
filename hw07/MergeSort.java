/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * A class that implement merge sort.
 *
 * @author weichin
 * @version  11/27/18
 */
public class MergeSort extends Sorter {
    Contributor [] temp;

    /**
     * A method that implement mergesort
     */
    @Override
    public void sort() 
    {
	temp = new Contributor[total];
        int start = 0;
        int end = total-1;
        mergeSort(conArray, start, end);

    }

    private void mergeSort(Contributor[] data, int start, int end) {
        if (start < end) {
            int middle = (start + end) / 2;             // find the middle
            mergeSort(data, start, middle);             // chop and sort the first half 
            mergeSort(data, middle + 1, end);       // chop and sort the second half
            merge(data, start, middle, end);       // merge the first half and second half when the array is completely splied into an individual elem
        }
    }

     private void merge(Contributor[] data, int start, int middle, int end) {
        int firstStartIndex = start;                        // define the first half starting index
        int secondStartIndex = middle+1;         // define the second half starting index
        int tempIndex = 0;                                   // define a tempIndex to do the copy

        while ((firstStartIndex<=middle) && (secondStartIndex<= end)) {        // if the index for first sub is within start and index for the second sub is within end then compare 
           if (data[firstStartIndex].compareTo(data[secondStartIndex]) >0) {                                   // if first element in first half is more than the first elem in second half
                temp[tempIndex++] = data[firstStartIndex++];                                // then in the temp array, the first elem will be that.
            }else{
                temp[tempIndex++] = data[secondStartIndex++];
            }
        }
        while (firstStartIndex <= middle) {                                                            // this code basically is copying the rest of the elem in data to temp
            temp[tempIndex++] = data[firstStartIndex++];                                        
        }
        while (secondStartIndex<= end) {                                                    // this is the other half
             temp[tempIndex++] = data[secondStartIndex++];
        }
        for (firstStartIndex = end; firstStartIndex >= start;firstStartIndex--) {
            data[firstStartIndex] = temp[--tempIndex];        
        
        }
    }
}
