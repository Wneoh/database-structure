
import java.io.FileNotFoundException;

/**
 *  A driver class that utilize built-in sort, selection sort and Merge sort and calculate the time.
 * @author weichin
 * @version 11/28/18
 */
public class Driver
{
	public static void main(String [] args) throws FileNotFoundException {
                                        Timer bisTime =new Timer();
                                        Timer ssTime = new Timer();
                                        Timer msTime =new Timer();
                                        
                                        BuiltInSort bis = new BuiltInSort();
                                        SelectionSort ss =new SelectionSort();
                                        MergeSort ms = new MergeSort();
                                        bis.load(args[0]);
                                        ss.load(args[0]);
                                        ms.load(args[0]);
					// built in sort time
                                        bisTime.start();
                                        bis.sort();
                                        bisTime.stop();
                                        //selection sort time
                                        ssTime.start();
                                        ss.sort();
                                        ssTime.stop();
                                        //merge sort time 
                                        msTime.start();
                                        ms.sort();
                                        msTime.stop();
                                        
                                        System.out.println("File name to sort	:"+args[0]);
                                        System.out.println("Built-in sort time	:"+bisTime.toString());
                                        bis.displayTop10();
					System.out.println("----------------------------------------");
					System.out.println("Selection sort time	:"+ssTime.toString());
					ss.displayTop10();
					System.out.println("----------------------------------------");
					System.out.println("Merger sort time	:"+msTime.toString());
                                        ms.displayTop10();

	}
}
