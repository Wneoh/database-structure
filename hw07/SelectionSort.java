/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * A class that inherit from Sorter class and implement selectionsort. 
 * @author weichin 
 * @version  11/27/18
 */
public class SelectionSort extends Sorter{

    /**
     * A method that implement selection sort
     */
    @Override
    public void sort() {
        for( int i =0;i<total-1;i++){  
            int max_index =i;                                                                       // set the max_index as the first element in the array. 
            for(int j =i+1;j<total;j++){                                                            // as we go through the loop, take the first element and compare 
                if(conArray[max_index].compareTo(conArray[j])<0){     // if other elem is bigger than the current 
                    max_index =j;                                                                  // set the max_index to that other elem position 
                }
            }
            Contributor temp = conArray[max_index];                         // swap the current and other positon.
            conArray[max_index] =conArray[i];
            conArray[i] = temp;
        }
    }
    
    
}
