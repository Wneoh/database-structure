/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;
import java.util.Collections;


/**
 * A class that implement the built in sort. 
 * @author weichin
 * @version  11/27/18
 */
public class BuiltInSort extends Sorter{

    /**
     * A method that implement the built in sort
     */
    @Override
    public void sort() {
        Arrays.sort(conArray,0,total-1,Collections.reverseOrder());
    }
    
}
