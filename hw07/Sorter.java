/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * An abstacted class that have the properties of read a file, load and sort it. 
 * @author weichin
 * @version 11/28/18
 */
public abstract class Sorter {
    	protected Contributor[]  conArray = new Contributor[2000000];
	protected int total;
    /**
     * A method that load file information into an object array
     * @param fileName
     * @throws FileNotFoundException
     */
    public void load(String fileName) throws FileNotFoundException{
        File file = new File(fileName);
        Scanner sr = new Scanner(file);
        while(sr.hasNextLine()){
            String line = sr.nextLine();
            String []details = line.split(" ");
            conArray[total++]= new Contributor(details[0],details[1],Integer.parseInt(details[2]));
	    
        }
    }
    /**
     * A method that display the Top 10 most income contributor
     */
    public void displayTop10(){
        for (int i =0; i<10;i++){
                System.out.println(""+conArray[i].toString());
        }
            
    }
    public abstract void sort();
}
